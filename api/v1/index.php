<?php

require 'Slim/Slim.php';
require 'php-jwt/JWT.php';
require 'php-jwt/BeforeValidException.php';
require 'php-jwt/ExpiredException.php';
require 'php-jwt/SignatureInvalidException.php';
\Slim\Slim::registerAutoloader();

use \Firebase\JWT\JWT;

$app = new \Slim\Slim();
$app->response->headers->set('Content-Type', 'application/json; charset=utf-8');
$app->post('/streets', function() use ($app) {
    require_once "bootstrap.php";
    require_once "Model/Claim.php";
    require_once "Model/Street.php";
    require_once "Model/User.php";
    require_once 'passwordHash.php';
    $key = "RNapUaRZbu9vPSUNGe6EACQs7XSzRV0kybuR2LJo";
    $r = json_decode($app->request->getBody());
    try {
        $authHead = $app->request->headers("Authorization");
        if ($authHead) {
            list($jwt) = sscanf($authHead, 'Bearer %s');
            if ($jwt) {
                try {
                    $decoded = JWT::decode($jwt, $key, array('HS256'));
                    $roles = $decoded->data->roles;
                    if (in_array("admin", $roles)) {
                        //echo 'Imate ispravan token';
                        $sifra = $r->streetCode;
                        $user_id = $r->readerId;
                        
                        $newStreet = new Street();
                        //$newStreet->setNaziv($naziv);
                        $newStreet->setSifra($sifra);
                        $user = $entityManager->find('User', $user_id);
                        $newStreet->setuser($user);
                        $entityManager->persist($newStreet);                     
                        $entityManager->flush();
                        //$serializer = JMS\Serializer\SerializerBuilder::create()->build();
                        //$jsonContent = $serializer->serialize($newUser, 'json');
                        $ResponseObj = new stdClass;
                        $ResponseObj->status = "success";
                        $ResponseObj->streetUser = $newStreet;
                        echoResponse(200, $ResponseObj);
                    } else {
                        $app->response()->setStatus(401);
                        echo '{"error":{"text":"Nemate ovlasti za dodavanje ulica"}}';
                    }
                } catch (Exception $e) {
                    $app->response()->setStatus(500);
                    error_log($e);
                    echo '{"error":{"text":"' . $e->getMessage() . '"}}';
                }
            } else {
                $app->response()->setStatus(401);
                echo '{"error":{"text":"Neispravna authorizacija. Bearer je samo dozvoljena"}}';
            }
        } else {
            $app->response()->setStatus(401);
            echo '{"error":{"text":"Nemate ovlasti - nema tokena"}}';
        }
    } catch (Exception $ex) {
        $response["status"] = "error";
        $response["message"] = 'Greska prilikom dodavanja ulice' . $ex->getMessage();
        echoResponse(500, $response);
    }
});

$app->get('/readers', function () {
    require_once "bootstrap.php";
   require_once "Model/Claim.php";
    require_once "Model/Street.php";
    require_once "Model/User.php";
    $userRepo = $entityManager->getRepository('User');
    $users = $userRepo -> findAll();
    //print_r($companies); 
    $serializer = JMS\Serializer\SerializerBuilder::create()->build();
    $jsonContent = $serializer->serialize($users, 'json');
    echo $jsonContent;
});
$app->post('/readers', function() use ($app) {
    require_once "bootstrap.php";
    require_once "Model/Claim.php";
    require_once "Model/Street.php";
    require_once "Model/User.php";
    require_once 'passwordHash.php';
    $key = "RNapUaRZbu9vPSUNGe6EACQs7XSzRV0kybuR2LJo";
    $r = json_decode($app->request->getBody());
    try {
        $authHead = $app->request->headers("Authorization");
        if ($authHead) {
            list($jwt) = sscanf($authHead, 'Bearer %s');
            if ($jwt) {
                try {
                    $decoded = JWT::decode($jwt, $key, array('HS256'));
                    $roles = $decoded->data->roles;
                    if (in_array("admin", $roles)) {
                        //echo 'Imate ispravan token';
                        $displayname = $r->displayName;
                        $username = $r->username;
                        $password = $r->password;
                        $newUser = new User;
                        $newUser->setSifra($username);
                        $newUser->setDisplayName($displayname);
                        $newUser->setEmail($username);
                        $newUser->setProjectName('water_readings');
                        $newUser->setUsername($username);
                        $newUser->setIsBlocked(0);
                        $newUser->setCompanyId(1);
                        $newUser->setPassword(passwordHash::hash($password));
                        $claim = new Claim();
                        $claim->setClaimType("role");
                        $claim->setClaimValue("reader");
                        $claim->setUser($newUser);
                        $entityManager->persist($claim);
                        $entityManager->persist($newUser);
                        $entityManager->flush();
                        //$serializer = JMS\Serializer\SerializerBuilder::create()->build();
                        //$jsonContent = $serializer->serialize($newUser, 'json');
                        $ResponseObj = new stdClass;
                        $ResponseObj->status = "success";
                        $ResponseObj->user = $newUser;
                        echoResponse(200, $ResponseObj);
                    } else {
                        $app->response()->setStatus(401);
                        echo '{"error":{"text":"Nemate ovlasti za dodavanje ocitavaca"}}';
                    }
                } catch (Exception $e) {
                    $app->response()->setStatus(500);
                    error_log($e);
                    echo '{"error":{"text":"' . $e->getMessage() . '"}}';
                }
            } else {
                $app->response()->setStatus(401);
                echo '{"error":{"text":"Neispravna authorizacija. Bearer je samo dozvoljena"}}';
            }
        } else {
            $app->response()->setStatus(401);
            echo '{"error":{"text":"Nemate ovlasti - nema tokena"}}';
        }
    } catch (Exception $ex) {
        $response["status"] = "error";
        $response["message"] = 'Greska prilikom dodavanja ocitavaca' . $ex->getMessage();
        echoResponse(500, $response);
    }
});
$app->post('/users', function() use ($app) {
    require_once "bootstrap.php";
    require_once "Model/Claim.php";
    require_once "Model/Street.php";
    require_once "Model/User.php";
    require_once 'passwordHash.php';
    $r = json_decode($app->request->getBody());
    try {

        $displayname = $r->user->displayname;
        $username = $r->user->username;
        $password = $r->user->password;

        $newUser = new User;
        $newUser->setSifra($username);
        $newUser->setDisplayName($displayname);
        $newUser->setEmail($username);
        $newUser->setProjectName('water_readings');
        $newUser->setUsername($username);
        $newUser->setIsBlocked(0);
        $newUser->setCompanyId(1);
        $newUser->setPassword(passwordHash::hash($password));
        $claim = new Claim();
        $claim->setClaimType("role");
        $claim->setClaimValue("admin");
        $claim->setUser($newUser);
        $entityManager->persist($claim);
        $entityManager->persist($newUser);
        $entityManager->flush();
        //$serializer = JMS\Serializer\SerializerBuilder::create()->build();
        //$jsonContent = $serializer->serialize($newUser, 'json');
        $ResponseObj = new stdClass;
        $ResponseObj->status = "success";
        $ResponseObj->user = $newUser;
        echoResponse(200, $ResponseObj);
    } catch (Exception $ex) {
        $response["status"] = "error";
        $response["message"] = 'Greska prilikom registracije ' . $ex->getMessage();
        echoResponse(500, $response);
    }
});
$app->post('/login', function() use ($app) {
    require_once "bootstrap.php";
    require_once "Model/Claim.php";
    require_once "Model/Street.php";
    require_once "Model/User.php";
    require_once 'passwordHash.php';
    $r = json_decode($app->request->getBody());
    try {
        $username = $r->user->username;
        $password = $r->user->password;
        $userRepo = $entityManager->getRepository('User');
        $userDb = $userRepo->findOneByUsername($username);
        if ($userDb) {
            $passwordDb = $userDb->getPassword();
            if (passwordHash::check_password($passwordDb, $password)) {
                $ResponseObj = new stdClass;
                $ResponseObj->status = "success";
                $ResponseObj->user = $userDb;
                $ResponseObj->token = createToken($userDb);

                echoResponse(200, $ResponseObj);
            } else {
                $response["status"] = "error";
                $response["message"] = 'NIje ispravni korisnicki podaci';
                echoResponse(401, $response);
            }
        } else {
            $response["status"] = "error";
            $response["message"] = 'NIje ispravni korisnicki podaci';
            echoResponse(401, $response);
        }
    } catch (Exception $ex) {
        $response["status"] = "error";
        $response["message"] = 'Greska prilikom prijave: ' . $ex->getMessage();
        echoResponse(401, $response);
    }
});

$app->get('/companies', function () {
    require_once "bootstrap.php";
    require_once "Model/Company.php";
    $companyRepo = $entityManager->getRepository('Company');
    $companies = $companyRepo->findAll();
    //print_r($companies); 
    $serializer = JMS\Serializer\SerializerBuilder::create()->build();
    $jsonContent = $serializer->serialize($companies, 'json');
    echo $jsonContent;
});
$app->get('/users', function () {
    require_once "bootstrap.php";
    require_once "Model/User.php";
    require_once "Model/Claim.php";
    $userRepo = $entityManager->getRepository('User');
    $users = $userRepo->findAll();
    //print_r($users);
    $serializer = JMS\Serializer\SerializerBuilder::create()->build();
    $jsonContent = $serializer->serialize($users, 'json');
    echo $jsonContent;
});
$app->get('/token', function () {
    $key = 'RNapUaRZbu9vPSUNGe6EACQs7XSzRV0kybuR2LJo';

    //TODO read basic auth from Athorization header usrrname and password
    //TODO Detect aplication name and company id from query string
    //TODO find user and his claims in database and construct token  
    $token = array(
        'iss' => 'http://issuer-of-jwt.org',
        'aud' => 'http://audience-resource.com',
        'iat' => 1356999524,
        'nbf' => 1357000000,
        'exp' => time() + 43200, //60expire in one min //3600 is hour //43200 12sati
        'sub' => 'batman',
        'scope' => ['read', 'write', 'delete'], //Try to make this for each entity
        'data' => [                  // Data related to the signer user
            'userId' => '3',
            'companyId' => '1',
            'projectName' => 'ocitanje',
            'username' => 'batman',
            'displayName' => 'batman lastname',
            'roles' => ['admin', 'user']
        ]
    );

    $jwt = JWT::encode($token, $key);

    echo $jwt;
});
$app->get('/secure', function () {
    //TODO get userId,companyId and project name from token
    //Check in table ocitanje which (or is record) satisfied with condition
    //Based on action read insert update delete from scope 
    //Entity action  
    $key = "RNapUaRZbu9vPSUNGe6EACQs7XSzRV0kybuR2LJo";
    $app = \Slim\Slim::getInstance();
    try {
        $authHead = $app->request->headers("Authorization");
        print_r($app->request()->headers());
        $company = $app->request->headers("Company");
        echo $company;
        if ($authHead) {
            list($jwt) = sscanf($authHead, 'Bearer %s');
            if ($jwt) {
                try {
                    $decoded = JWT::decode($jwt, $key, array('HS256'));
                    //echo 'Imate ispravan token';
                    echo '{"Username": "' . $decoded->data->username . '","User Id":"' . $decoded->data->userId . '"}';
                } catch (Exception $e) {
                    $app->response()->setStatus(401);
                    error_log($e);
                    echo '{"error":{"text":"token neispravan"}}';
                }
            } else {
                $app->response()->setStatus(401);
                echo '{"error":{"text":"Neispravna authorizacija. Bearer je samo dozvoljena"}}';
            }
        } else {
            $app->response()->setStatus(401);
            echo '{"error":{"text":"Nemate ovlasti - nema tokena"}}';
        }
    } catch (Exception $exc) {
        echo '{"error":{"text":' . $exc->getMessage() . '}}';
    }
});

function verifyRequiredParams($required_fields, $request_params) {
    $error = false;
    $error_fields = "";
    foreach ($required_fields as $field) {
        if (!isset($request_params->$field) || strlen(trim($request_params->$field)) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["status"] = "error";
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoResponse(200, $response);
        $app->stop();
    }
}

function echoResponse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

function createToken($user) {
    $roles = array();
    $claims = array();
    $key = 'RNapUaRZbu9vPSUNGe6EACQs7XSzRV0kybuR2LJo';

    $userClaims = $user->getClaims();
    foreach ($userClaims as $claim) {
        if ($claim->getClaimType() == 'role') {
            $roles[] = $claim->getClaimValue();
        } else {
            $claims[$claim->getClaimType()] = $claim->getClaimValue();
        }
    }
    $token = array(
        'iss' => 'http://www.smart-vodovod.com/',
        'aud' => 'http://www.smart-vodovod.com/',
        'iat' => 1356999524,
        'nbf' => 1357000000,
        'exp' => time() + 43200, //60expire in one min //3600 is hour //43200 12sati
        'sub' => $user->getUsername(),
        //'scope' => ['read', 'write', 'delete'],//Try to make this for each entity
        'data' => [                  // Data related to the signer user
            'userId' => $user->getId(),
            'companyId' => $user->getCompanyId(),
            'projectName' => $user->getProjectName(),
            'username' => $user->getUsername(),
            'displayName' => $user->getUsername(),
            'roles' => $roles,
            'claims' => $claims,
        ]
    );

    $jwt = JWT::encode($token, $key);
    return $jwt;
}

$app->run();
