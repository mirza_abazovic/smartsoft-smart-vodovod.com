<?php
/**
 * Created by PhpStorm.
 * User: am
 * Date: 26.8.2015.
 * Time: 8:33
 */

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="claim")
 */
class Claim implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\Column(name="claim_type",type="string", length=255)*/
    private $claimType;

    /** @ORM\Column(name="claim_value",type="string", length=255)*/
    private $claimValue;


    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="claims",cascade={"persist"})
     */
    protected $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getClaimType()
    {
        return $this->claimType;
    }

    /**
     * @param mixed $claimType
     */
    public function setClaimType($claimType)
    {
        $this->claimType = $claimType;
    }

    /**
     * @return mixed
     */
    public function getClaimValue()
    {
        return $this->claimValue;
    }

    /**
     * @param mixed $claimValue
     */
    public function setClaimValue($claimValue)
    {
        $this->claimValue = $claimValue;
    }

/**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $id
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


    public function jsonSerialize() {
         return array(
             //'id' => $this->getId(),
             'type' => $this->getClaimType(),
             'value' => $this->getClaimValue(),
        );
    }

}