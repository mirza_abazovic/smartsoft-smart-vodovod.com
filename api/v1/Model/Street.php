<?php
/**
 * Created by PhpStorm.
 * User: am
 * Date: 26.8.2015.
 * Time: 8:33
 */

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="street")
 */
class Street implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\Column(name="sifra",type="string", length=255)*/
    private $sifra;

    /** @ORM\Column(name="naziv",type="string", length=255)*/
    private $naziv;

  

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="streets")
     */
    protected $user;

    /**
     * @return mixed
     */
    public function getNaziv()
    {
        return $this->naziv;
    }

    /**
     * @param mixed $naziv
     */
    public function setNaziv($naziv)
    {
        $this->naziv = $naziv;
    }

    /**
     * @return mixed
     */
    public function getSifra()
    {
        return $this->sifra;
    }

    /**
     * @param mixed $sifra
     */
    public function setSifra($sifra)
    {
        $this->sifra = $sifra;
    }
 /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setuser($user)
    {
        $this->user = $user;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function jsonSerialize() {
         return array(
             'id' => $this->getId(),
             'sifra' => $this->getSifra(),
             'naziv' => $this->getNaziv()
        );
    }

}