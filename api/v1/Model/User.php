<?php
/**
 * Created by PhpStorm.
 * User: am
 * Date: 26.8.2015.
 * Time: 8:33
 */

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @ExclusionPolicy("none")
 */

class User implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")   
     */
    private $id;

    /** 
     * @ORM\Column(type="string", length=255)
     */
    private $sifra;

    /** 
     * @ORM\Column(type="string", length=255)     
     */
    private $username;

    /** @ORM\Column(name="display_name", type="string", length=255)*/
    private $displayName;

    /** @ORM\Column(type="string", length=100)*/
    private $email;

    /** 
     * @ORM\Column(type="string", length=100)
     * @Exclude     
     */
    private $password;

    /** @ORM\Column(name="is_blocked",type="integer")*/
    private $isBlocked;

    /** @ORM\Column(name="project_name",type="string", length=100)*/
    private $projectName;

    /** @ORM\Column(name="company_id",type="integer")*/
    private $companyId;



    /**
     * @var Claim[]
     *
     * @ORM\OneToMany(targetEntity="Claim", mappedBy="user")
     */
    protected $claims;

    /**
     * @var Street[]
     *
     * @ORM\OneToMany(targetEntity="Street", mappedBy="user")
     */
    protected $streets;

    /**
     * @return Street[]
     */
    public function getStreets()
    {
        return $this->streets;
    }

    /**
     * @param Street[] $streets
     */
    public function setStreets($streets)
    {
        $this->streets = $streets;
    }


    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @param mixed $companyId
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSifra()
    {
        return $this->sifra;
    }

    /**
     * @param mixed $sifra
     */
    public function setSifra($sifra)
    {
        $this->sifra = $sifra;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @param mixed $displayName
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getIsBlocked()
    {
        return $this->isBlocked;
    }

    /**
     * @param mixed $isBlocked
     */
    public function setIsBlocked($isBlocked)
    {
        $this->isBlocked = $isBlocked;
    }

    /**
     * @return mixed
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * @param mixed $projectName
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }

    /**
     * @return mixed
     */
    public function getClaims()
    {
        return $this->claims;
    }

    /**
     * @param mixed $claims
     */
    public function setClaims($claims)
    {
        $this->claims = $claims;
    }
    public function jsonSerialize() {
        
        return array(
             'id' => $this->getId(),
             'username' => $this->getUsername(),
             'displayName' => $this->getDisplayName(),
             'email' => $this->getEmail(),
             //'sifra' => $this->getSifra(),
             'isBlocked' => $this->getIsBlocked(),
             'projectName' => $this->getProjectName()
             //'claims' => $claimsJson,
             //'streets' => $streetsJson,
        );
    }

}