<?php
/**
 * Created by PhpStorm.
 * User: am
 * Date: 26.8.2015.
 * Time: 8:33
 */

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="company")
 */
class Company implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
   
    /** @ORM\Column(type="string", length=255)*/
    private $name;


    /** @ORM\Column(type="string", length=255)*/
    private $address;

     /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    
    public function jsonSerialize() {
         return array(
             'id' => $this->getId(),
             'name' => $this->getName(),
             'address' => $this->getAddress(),
        );
    }

}