<?php

require_once 'vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$paths = array("/Model");
$isDevMode = false;
$isSimpleMode = FALSE;
$proxyDir = null;
$cache = null;


// the connection configuration
$dbParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'smartvod_ad',
    'password' => '0202Mirza1978',
    'dbname'   => 'smartvod_readings',
    'host'     => 'smart-vodovod.com',
    'port'     => '3306',
     'charset' => 'utf8',
     'driverOptions' => array(1002=>'SET NAMES utf8')
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, $proxyDir, $cache, $isSimpleMode);
$entityManager = EntityManager::create($dbParams, $config);

\Doctrine\Common\Annotations\AnnotationRegistry::registerAutoloadNamespace(
    'JMS\Serializer\Annotation', __DIR__.'/vendor/jms/serializer/src'
);