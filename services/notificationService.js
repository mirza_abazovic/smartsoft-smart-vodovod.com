angular.module('app.services')
        .factory('NotificationService', notificationService);
function notificationService() {
    var data = {
        'success': success,
        'error': error,
        'warning': warning
    };
    toastr.options.closeButton = true;
    toastr.options.positionClass = "toast-bottom-right";

    function success(text) {
        toastr.success(text);
    }
    ;
    function error(text) {
        toastr.error(text);
    }
    ;
    function warning(text) {
        toastr.warning(text);
    }
    ;
    return data;
}

