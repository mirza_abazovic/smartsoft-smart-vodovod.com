angular.module('app.services')
        .constant('BASE_URL', 'api/v1/')
        .factory('ReadingsService', readingsService);

function readingsService($http, $log, $window, BASE_URL,NotificationService) {
    var data = {
        'addReader': addReader,
        'mapReaderToStreet': mapReaderToStreet,
        'getReaders': getReaders,
        'registerUser': registerUser,
        'login': login,
        'saveToken': saveToken,
        'getToken': getToken,
        'saveUser': saveUser,
        'getUser': getUser,
        'isAuthenticated': isAuthenticated,
        'logout': logout
    };
    function mapReaderToStreet(readerId,streetCode){
        data = {"readerId":readerId,"streetCode":streetCode};
        var requestUrl = BASE_URL + 'streets';
        return $http({
            'url': requestUrl,
            'method': 'POST',
            'data': data,
            'headers': {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+getToken()
            },
            'cache': true
        }).then(function (response) {
            response.sucess = true;
            //NotificationService.success("Uspjesno je dodata osoba za ocitavanje");
            return response.data;
        }).catch(dataServiceError);
    
    }
    function getReaders() {
        //alert('U factory servisu registerUSer');
        var requestUrl = BASE_URL + 'readers';
        return $http({
            'url': requestUrl,
            'method': 'GET',
            'headers': {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+getToken()
            }
        });
    }
    function getTokenOld(username, password) {
        var requestUrl = BASE_URL + 'token';
        var tok = username + ':' + password;
        var authHeader = 'Basic ' + Base64.encode(tok);
        $http({
            'url': requestUrl,
            method: 'GET', headers: {
                'Authorization': authHeader
            }
        }).then(function (response) {
            response.sucess = true;
            
            return response.data;
        }).catch(dataServiceError);
    }
    function addReader(reader) {
        //alert('U factory servisu registerUSer');
        var requestUrl = BASE_URL + 'readers';
        return $http({
            'url': requestUrl,
            'method': 'POST',
            'data': reader,
            'headers': {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+getToken()
            },
            'cache': true
        }).then(function (response) {
            response.sucess = true;
            //NotificationService.success("Uspjesno je dodata osoba za ocitavanje");
            return response.data;
        }).catch(dataServiceError);
    }
    function registerUser(user) {
        //alert('U factory servisu registerUSer');
        var requestUrl = BASE_URL + 'users';
        return $http({
            'url': requestUrl,
            'method': 'POST',
            'data': user,
            'headers': {
                'Content-Type': 'application/json'
            },
            'cache': true
        }).then(function (response) {
            response.sucess = true;
            return response.data;
        }).catch(dataServiceError);
    }
    function login(user) {
        var requestUrl = BASE_URL + 'login';
        return $http({
            'url': requestUrl,
            'method': 'POST',
            'data': user,
            'headers': {
                'Content-Type': 'application/json'
            },
            'cache': true
        }).then(function (response) {
            response.sucess = true;
            return response.data;
        }).catch(dataServiceError);
    }
    function getToken() {
        return $window.localStorage['jwtToken'];
    }
    function saveToken(token) {
        $window.localStorage['jwtToken'] = token;
    }
    function getUser() {
        var user = $window.localStorage['user'];
        if (user != null) {
            return JSON.parse(user);
        }
        else {
            return null;
        }
    }
    function saveUser(user) {
        $window.localStorage['user'] = JSON.stringify(user);
    }
    function isAuthenticated() {
        var token = getToken();
        if (token) {
            return true;
        }
        else {
            return false;
        }
    }
    function logout() {
        $window.localStorage.clear();
    }
    function dataServiceError(errorResponse) {
        $log.error('XHR Failed for ShowService');
        $log.error(errorResponse);
        errorResponse.sucess = false;
        return errorResponse;
    }
    return data;
}