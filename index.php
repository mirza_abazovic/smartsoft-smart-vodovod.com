<!DOCTYPE html>
<html lang="en" ng-app="readings"
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>SMARTSOFT VODOVOD</title>
        <!-- Bootstrap -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Angular toastr -->
        <link href="bower_components/AngularJS-Toaster/toaster.css" rel="stylesheet">
        <!-- Font awesome -->
        <link href="bower_components/font-awesome/css/font-awesome.min.css"  rel="stylesheet">
        <!-- Toastr -->
        <link href="bower_components/toastr/toastr.min.css" rel="stylesheet">
        <!-- Custom -->
        <link href="css/custom.css" rel="stylesheet">
    </head>
    <body >
        <nav class="navbar" ng-controller="HomeController as home">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"></a>
                </div>
                <div>
                    <ul class="nav navbar-nav">
                        <!--
                           <li>
                           <a class="navbar-brand" rel="home" title="Ulice" href="#/streets">Ulice</a>
                         </li>
                        -->
                        <li>
                            <a class="navbar-brand" rel="home" title="Očitavači" href="#/users">Očitavači</a>
                        </li>
                        <li>
                            <a class="navbar-brand" rel="home" title="Dodjela ulica očitavačima" href="#/streets-users">Dodjela ulica očitavačima</a>
                        </li>
                        <li>
                            <a class="navbar-brand" rel="home" title="Novo očitanje" href="#/new-readings">Novo očitanje</a>
                        </li>
                        <li>
                            <a class="navbar-brand" rel="home" title="Trenutno očitanja" href="#/current-readings">Trenutno očitanje</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li ng-show="home.isAuthenticated"><a href>Prijavljeni korisnik: {{home.user.displayName}}</a></li>
                        <li ng-show="!home.isAuthenticated">
                            <a href="#/register">
                                <span class="glyphicon glyphicon-user"></span> Registracija</a>
                        </li>
                        <!--                        <li  ng-show="!home.isAuthenticated">
                                                    <a href="#/login"><span class="glyphicon glyphicon-log-in"></span> Prijava</a>
                                                </li>-->
                        <li ng-show="home.isAuthenticated">
                            <a href ng-click="home.logout()"><span class="glyphicon glyphicon-log-out"></span> Odjava</a>
                        </li>
                        <form name="signupForm" ng-show="!home.isAuthenticated" class="navbar-form navbar-right">
                            <div class="form-group">
                                <input type="text" ng-model="login.user.username" placeholder="Korisničko ime" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <input type="password" ng-model="login.user.password" placeholder="Lozinka" class="form-control" required>
                            </div>
                            <button type="submit" ng-click="home.login(login.user)" ng-disabled="signupForm.$invalid" class="btn   btn-primary">Prijava</button>
                        </form>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container" style="margin-top:20px;">
            <div ng-view="" id="ng-view" ></div> <!--class="view-animation"-->
        </div>
        <div class="footer">
            <div class="container">
                <p>&copy; 2015 - SmartSoft</p>

            </div>
<!--            <footer>
                <p>&copy; 2015 - SmartSoft</p>
            </footer>-->

    </body>
    <toaster-container toaster-options="{'time-out': 3000}"></toaster-container>
    <!-- Libs -->
    <script src="bower_components/angular/angular.min.js"></script>
    <script src="bower_components/angular-route/angular-route.min.js"></script>
    <script src="bower_components/angular-animate/angular-animate.min.js" ></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/toastr/toastr.min.js"></script>
    <script src="app/app.js"></script>
    <script src="app/app.core.js"></script>
    <script src="app/app.routes.js"></script>
    <script src="app/app.services.js"></script>
    <script src="app/app.filters.js"></script>
    <script src="app/app.directives.js"></script>
    <!--SERVICES-->
    <script src="services/readingsService.js"></script>
    <script src="services/notificationService.js"></script>
    
    <!--CONTROLLERS-->
    <script src="sections/register/registerController.js"></script>
    <script src="sections/login/loginController.js"></script>
    <script src="sections/home-menu/homeMenuController.js"></script>
    <script src="sections/home/homeController.js"></script>
    <script src="sections/streets/streetsController.js"></script>
    <script src="sections/users/usersController.js"></script>
    <script src="sections/streets-users/streetsUsersController.js"></script>
    <script src="sections/current-readings/currentReadingsController.js"></script>
    <script src="sections/new-readings/newReadingsController.js"></script>
</html>