angular.module('app.filters', [])
        .filter('stringToDate', function () {
            return function (input) {
                if (typeof input == "undefined") {
                    return "";
                }
                var output = input.substring(8, 10) + '.' + input.substring(5, 7) + '.' + input.substring(0, 4);
                //"2015-04-27T11:28:26.350Z"
                return output;

            }
        })
        .filter('stringToTime', function () {
            return function (input) {
                var output = input.substring(11, 19);
                //"2015-04-27T11:28:26.350Z".substring(11,19)
                return output;

            }
        })
        .filter('stringToDateTime', function () {
            return function (input) {
                if (typeof input == "undefined") {
                    return "";
                }
                var output = input.substring(8, 10) + '.' + input.substring(5, 7) + '.' + input.substring(0, 4) + ' ' + input.substring(11, 16);
                return output;
            }
        });