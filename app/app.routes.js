angular
    .module('app.routes', ['ngRoute'])
    .config(routes)
    .run(function($location, $rootScope, ReadingsService) {
    $rootScope.$on('$routeChangeStart', function(event, next, current) {
        if (!ReadingsService.isAuthenticated()) {
            var nextUrl = next.$$route.originalPath;
            if (nextUrl == '/register' || nextUrl == '/login' )
            {

            } 
            else {
                $location.path("/login");
            }
        }
    });
    });


function routes($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'sections/home-menu/home-menu.html',
            controller: 'HomeMenuController',
            controllerAs: 'homeMenu'
        })
        .when('/register', {
            templateUrl: 'sections/register/register.html',
            controller: 'RegisterController',
            controllerAs: 'register'
        }).
        when('/login', {
            templateUrl: 'sections/login/login.html',
            controller: 'LoginController',
            controllerAs: 'login'
        })
//        .when('/streets', {
//            templateUrl: 'sections/streets/streets.html',
//            controller: 'StreetsController',
//            controllerAs: 'streets'
//        })
        .when('/users', {
            templateUrl: 'sections/users/users.html',
            controller: 'UsersController',
            controllerAs: 'users'
        })
        .when('/streets-users', {
            templateUrl: 'sections/streets-users/streets-users.html',
            controller: 'StreetsUsersController',
            controllerAs: 'streetsUsers'
        })
        .when('/current-readings', {
            templateUrl: 'sections/current-readings/current-readings.html',
            controller: 'CurrentReadingsController',
            controllerAs: 'currentReadings'
        })
        .when('/new-readings', {
            templateUrl: 'sections/new-readings/new-readings.html',
            controller: 'NewReadingsController',
            controllerAs: 'newReadings'
        })
        .otherwise({
            redirectTo: '/login'
        });
}