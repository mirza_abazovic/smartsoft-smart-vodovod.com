angular.module('app.core')
        .controller('LoginController',function(ReadingsService,NotificationService,$location){
    var vm = this;
    vm.user = {username: '', password: ''};
    vm.login = function (user) {
        ReadingsService.login({
            user: user
        }).then(function (results) {
            if(results.status==='success'){
                vm.user = {username: '', password: ''};
                ReadingsService.saveToken(results.token);
                ReadingsService.saveUser(results.user);
                 $location.path("/");
                 NotificationService.success("Prijava uspješna");
            }
            else{
                 NotificationService.error("Prijava neuspješna. Probajte ponovo.");
                
            }
         });
    };
});
