angular.module('app.core').controller('CurrentReadingsController', function () {
    var vm = this;
    vm.search = {};
    vm.ocitanja = [];

    vm.getTotal = function () {
        var total = 0;
        for (var i = 0; i < vm.ocitanja.length; i++) {
            var ocitanje = vm.ocitanja[i];
            total += parseInt(ocitanje.attributes.zadnjeStanjeBrojila);
        }
        return total;
    }

    vm.calculateTotalPrethodno = function (o) {
        var total = 0;
        for (var i = 0; i < vm.filteredOcitanja.length; i++) {
            var ocitanje = vm.filteredOcitanja[i];
            total += parseInt(ocitanje.attributes.zadnjeStanjeBrojila);
        }
        return total;
    }

    vm.calculateTotalNovo = function (o) {
        var total = 0;
        for (var i = 0; i < vm.filteredOcitanja.length; i++) {
            var ocitanje = vm.filteredOcitanja[i];
            if (ocitanje.attributes.novoOcitanje) {
                total += parseInt(ocitanje.attributes.novoOcitanje);
            }
        }
        return total;
    }
    vm.set_color = function (ocitanje) {
        if (!ocitanje || ocitanje === 0) {
            return {color: "red"}
        }
    }

    vm.getOcitanja = function () {
        Parse.Cloud.run('ocitanja', {}, {
            success: function (results) {
                vm.ocitanja = results;
                //mapParseOcitanja(results);
                vm.$apply();
                toastr.success('Ocitanja dobijena sa servera');
            },
            error: function (error) {
                toastr.error('Greska prilikom dobijanja ocitanja sa servera');
            }
        });
    }
    vm.downloadOcitanja = function () {
        var rawData = new Array();
        var output = '';
        var rows = document.getElementsByTagName('tr');
        for (var cnt = 1; cnt < rows.length - 1; cnt++) {
            var cells = rows[cnt].getElementsByTagName('td');
            var row = [];
            for (var count = 0; count < cells.length; count++) {
                row.push(cells[count].innerText.trim());
            }
            rawData.push(row);
        }
        var len = rawData.length;
        for (var i = 0; i < len; i++) {
            for (var j = 0; j < rawData[i].length; j++) {
                //if (rawData[i][7] == "true") {
                if (j === 3 || j === 6 || j === 9) {
                    if (j === 9) {
                        output += rawData[i][j].replace(".", "");
                        ;
                    }
                    else {
                        output += rawData[i][j];
                    }

                    output += '\t';
                }
                // }

            }
            output += '\r\n'
        }
        var danas = new Date();
        var mjesec = 1;
        mjesec = mjesec + danas.getMonth();
        var txtFilename = 'ocitanje_' + danas.getDate() + '.' + mjesec + '.' + danas.getFullYear();
        if (vm.search.attributes) {
            if (vm.search.attributes.sifraZone) {
                txtFilename += '_zona_' + vm.search.attributes.sifraZone;
            }
            if (vm.search.attributes.sifraVodomjera) {
                txtFilename += '_vodomjer_' + vm.search.attributes.sifraVodomjera;
            }
        }
        txtFilename = txtFilename + '.txt';
        download(txtFilename, output);
    }

    function download(filename, text) {
        var pom = document.createElement('a');
        //pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + escape(text));
        pom.setAttribute('download', filename);
        if (document.createEvent) {
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            pom.dispatchEvent(event);
        }
        else {
            pom.click();
        }
    }

});