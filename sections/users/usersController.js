angular.module('app.core').controller('UsersController',function(ReadingsService,NotificationService){
    var vm = this;
    vm.newUser ={"id":0,"displayName": "", "username": "", "password":""};
    
    vm.addUser = function () {
        ReadingsService.addReader(vm.newUser);
        vm.newUser = {};
        getUsers();
    };
     vm.deleteUser = function (id) {
        NotificationService.success(id);
        getUsers();
    };
    function getUsers() {
        var users = ReadingsService.getReaders().success(function (data) {
                vm.usersList = data;
            });
        
    };  
    getUsers();
});