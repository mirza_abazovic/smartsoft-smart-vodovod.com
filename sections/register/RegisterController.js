angular.module('app.core').controller('RegisterController',function(ReadingsService,NotificationService,$location) {
    var vm = this;
    vm.user = {username: '', password: ''};
    vm.register = function (user) {
        ReadingsService.registerUser({
            user: user
        }).then(function (results) {
            console.log(results);
            if(results.status === "success"){
                NotificationService.success("Registracija uspješna. Molimo prijavite se.");
                $location.path("/login");
            }
            else{
                NotificationService.error('Desila se greška prilikom registracije.');
            }
        });
    };
});

