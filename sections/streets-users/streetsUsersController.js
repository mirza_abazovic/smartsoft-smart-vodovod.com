angular.module('app.core').controller('StreetsUsersController',function(NotificationService,ReadingsService){
    var vm = this;
    vm.street = { sifraUlice: "", naziv: "" };
    vm.streets = [{"sifraUlice": "001", "username": "u1"},{"sifraUlice": "002", "username": "u2"}];
    vm.selectedUserId = null;
    vm.usersList = [];
    var getStreetsUsers = function () {
      return vm.streets;
    };
   
    function getUsers() {
        var users = ReadingsService.getReaders().success(function (data) {
                vm.usersList = data;
            });
        
    };  
   getUsers();
   getStreetsUsers();
    
    vm.add = function () {
        ReadingsService.mapReaderToStreet(vm.selectedUserId,vm.street.sifraUlice)
    };
    vm.delete = function (streetId) {
        
    };
});