angular.module('app.core').controller('HomeMenuController', function (ReadingsService) {
    var vm = this;
    var user = ReadingsService.getUser();
    if (user) {
        vm.user = ReadingsService.getUser();
    }
});
