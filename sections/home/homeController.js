angular.module('app.core').controller('HomeController', function (ReadingsService, NotificationService, $scope, $location) {
    var vm = this;
    vm.user = {username: '', password: ''};
    var user = ReadingsService.getUser();
    if (user) {
        vm.user = ReadingsService.getUser();
        vm.isAuthenticated = true;
    }
    else{
          vm.user = {username: '', password: ''};
    }
    $scope.$watch(ReadingsService.isAuthenticated, function (newVal, oldVal) {
        vm.isAuthenticated = newVal;
        if (newVal) {
            vm.user = ReadingsService.getUser();
        }
    });
    vm.logout = function () {
        ReadingsService.logout();
        vm.isAuthenticated = false;
        $location.path("/login");
    };
    vm.login = function (user) {
        ReadingsService.login({
            user: user
        }).then(function (results) {
            if (results.status === 'success') {
              
                ReadingsService.saveToken(results.token);
                ReadingsService.saveUser(results.user);
                $location.path("/");
                NotificationService.success("Prijava uspješna");

            }
            else {
                NotificationService.error("Prijava neuspješna. Probajte ponovo.");

            }
        });
    };
});
